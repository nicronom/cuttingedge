const defaultUrl = "http://localhost:8080/availability/byName?name=";

const checkUserSubscribed = async() => {
    var url = "http://localhost:8080/users/byEmail?email=" + sessionStorage.getItem("userEmail");
    var user;

    if (sessionStorage.getItem("userEmail") !== null) {
        await axios.get(url).
        then(response => {
            user = response.data;
            if (user.subscribed === true) {
                sessionStorage.setItem("isSubscribed", "true");
            } else {
                sessionStorage.setItem("isSubscribed", "false");
            }
            
        }).catch(sessionStorage.setItem("isSubscribed", "false"))
    }
}

var statuses = [];

// the function which handles booking a given timeslot
const makeBooking = async(bookingObj, idNumber) => {
    // the respective message of that timeslot div
    var message = document.getElementById(idNumber.toString());
    // the default error message if no error occurs
    message.innerText = "Booking successful";

    var url = 'http://localhost:8080/bookings/bookNew';
    await axios.post(url, bookingObj)
    .catch(function (error) {
        // the error message if unsuccessful
        message.innerText = "This timeslot is fully booked.";
        console.log(error);
    });

    // in all cases the message needs to be displayed
    message.setAttribute("style", "visibility: visible;");

    var theButton = document.getElementById(idNumber.toString() + idNumber.toString());
    theButton.innerText = "Cancel Book";
    theButton.setAttribute("style", "background-color: red; color: white; text-decoration: none;")
}

// the handler for cancelling a given booked timeslot for that user
const cancelBooking = async(bookingObj, idNumber) => {
    var message = document.getElementById(idNumber.toString());
    message.innerText = "Booking was cancelled";

    var url = 'http://localhost:8080/bookings/removeBook';
    await axios.delete(url, {
        params: {
            fromHour: bookingObj.availability.fromHour,
            untilHour: bookingObj.availability.untilHour,
            availableDay: bookingObj.availability.availableDay,
            email: bookingObj.email
        }
    })
    .catch(function (error) {
        message.innerText = "Something went wrong.";
        console.log(error);
    });

    message.setAttribute("style", "visibility: visible;");
    var theButton = document.getElementById(idNumber.toString() + idNumber.toString());
    theButton.innerText = "Book";
    theButton.setAttribute("style", "background-color: #008CBA; color: white; text-decoration: none;")
}

// gets the booked timeslots for the logged in user
const getUserBookings = async(reqUrl) => {
    var bookings;
    await axios({
        method: 'get',
        url: reqUrl,
        responseType: 'json'
    })
    .then(response => {
        bookings = response.data;
    })
    .catch(
        error => console.log(error)
    )

    return bookings;
}

// checks if the timeslots have already been booked by the user to render the buttons appropriately
const checkMatchingBook = async(avail) => {
    var url = 'http://localhost:8080/bookings/byEmail?email=' + sessionStorage.getItem("userEmail");
    var bookings = await getUserBookings(url);

    if ((bookings === null) || (bookings === undefined) || (bookings.length === undefined)) {
        return false;
    } else {
        for (let i=0; i < bookings.length; i++) {
            if (bookings[i].fromHour === avail.fromHour &&
                bookings[i].untilHour === avail.untilHour && 
                bookings[i].availableDay === avail.availableDay) {
                    return true;
                }
        }
    }

    return false;

}

// fetches which timeslots are currently available for the clicked activity
const fetchAvailabilities = async() => {
    var availabilities;
    await axios.get(defaultUrl + sessionStorage.getItem("activityName"))
    .then(response => {
        availabilities = response.data;
    })
    .catch(error => console.log(error))

    return availabilities;
}

// handles all the rendering of the page
const showAvailabilities = async() => {
    await checkUserSubscribed();
    var availabilities = await fetchAvailabilities();
    
    // the container div for this page
    var divElt = document.getElementById("times");

    // display a helpful message if there are no timeslots for this activity
    if (availabilities === null 
        || availabilities === undefined 
        || availabilities.length === undefined
        || availabilities.length === 0) {
            var displayMessage = document.createElement("h3");
            displayMessage.innerText = "No timeslots are currently available for " 
                                        + sessionStorage.getItem("activityName"); 
            
            divElt.appendChild(displayMessage);
        }
    else {
        for (let i=0; i < availabilities.length; i++) {
            // create a container for each availability and render similarly for all
            var cardDiv = document.createElement("div");
            cardDiv.setAttribute("style", "text-align: center; font-size: large; border-bottom: 4px solid orange;");

            var timeslotHeader = document.createElement("h5");
            timeslotHeader.setAttribute("style", "color: orange");
            timeslotHeader.innerText = availabilities[i].availableDay;

            var timeslotHours = document.createElement("p");
            timeslotHours.setAttribute("style", "color: orange");
            timeslotHours.innerText = availabilities[i].fromHour 
                                    + " " + availabilities[i].untilHour;

            var bookMessage = document.createElement("p");
            bookMessage.setAttribute("style", "visibility: hidden;");
            bookMessage.setAttribute("id", i.toString());
            
            var timeslotButton = document.createElement("a");
            timeslotButton.setAttribute("id", i.toString() + i.toString());
            // the timeslot button either books a timeslot or has the option to cancel it
            timeslotButton.onclick = async() => {
                
                avail = {
                    fromHour: availabilities[i].fromHour,
                    untilHour: availabilities[i].untilHour,
                    availableDay: availabilities[i].availableDay
                }
                bookingObj = {
                    availability: avail,
                    email: sessionStorage.getItem("userEmail")
                }

                var state = statuses[i];
                
                if (sessionStorage.getItem("isSubscribed") === "true") {
                    if (state === "notBooked") { 
                        await makeBooking(bookingObj, i);
                        statuses[i] = "booked";

                    }  else {
                        await cancelBooking(bookingObj, i);
                        statuses[i] = "notBooked";
                    }
                }
            }

            // in order to render the button appropriately, 
            // the timeslot needs to be checked if it was already booked.
            var isBooked = await checkMatchingBook({
                fromHour: availabilities[i].fromHour,
                untilHour: availabilities[i].untilHour,
                availableDay: availabilities[i].availableDay
            });
            if (isBooked === true) {
                statuses.push("booked");
            } else {
                statuses.push("notBooked");
            }

            timeslotButton.setAttribute("href", "#");

            if (statuses[i] === "booked") {
                timeslotButton.innerText = "Cancel Book";
                timeslotButton.setAttribute("style", "background-color: red; color: white; text-decoration: none;")
            }  else {
                timeslotButton.innerText = "Book";
                timeslotButton.setAttribute("style", "background-color: #008CBA; color: white; text-decoration: none;");
            }

            if (sessionStorage.getItem("userEmail") === "" || sessionStorage.getItem("isLoggedIn") === 'false' || sessionStorage.getItem("userEmail") === null) {
                timeslotButton.innerText = "Book";
                timeslotButton.setAttribute("style", "background-color: #008CBA; color: white; text-decoration: none;");
                timeslotButton.setAttribute("href", "SignIn.html");
            } else if (sessionStorage.getItem("isSubscribed") === "false" 
                        && !(sessionStorage.getItem("userEmail") === "" 
                                || sessionStorage.getItem("isLoggedIn") === 'false') 
                                || sessionStorage.getItem("userEmail") === null) {
                            timeslotButton.innerText = "Book";
                            timeslotButton.setAttribute("style", "background-color: #008CBA; color: white; text-decoration: none;");
                            timeslotButton.setAttribute("href", "membership.html");
            }

            cardDiv.appendChild(timeslotHeader);
            cardDiv.appendChild(timeslotHours);
            cardDiv.appendChild(timeslotButton);
            cardDiv.appendChild(bookMessage);

            divElt.appendChild(cardDiv);
        }
    }
}

async function handleTimeslots() {
    await showAvailabilities();
}

handleTimeslots();
