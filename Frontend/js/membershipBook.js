const checkUserSubscribed = async() => {
    var url = "http://localhost:8080/users/byEmail?email=" + sessionStorage.getItem("userEmail");
    var user;

    await axios.get(url)
    .then(response => {
        user = response.data;
        if (user.subscribed === true) {
            sessionStorage.setItem("isSubscribed", "true");
        } else {
            sessionStorage.setItem("isSubscribed", "false");
        }
        
    }).catch(sessionStorage.setItem("isSubscribed", "false"))
}

checkUserSubscribed();