function renderVisibles() {
    var imgSrc = sessionStorage.getItem("activityImg");
    var info = sessionStorage.getItem("activityInfo");

    var divElt = document.createElement("div");
    divElt.setAttribute("class", "slideshow-container");

    var imgElt = document.createElement("img");
    imgElt.setAttribute("src", imgSrc);
    imgElt.setAttribute("style", "width:100%");
    // TODO: perhaps add additional information within the image here
    divElt.appendChild(imgElt);

    var infoDivElt = document.createElement("div");
    infoDivElt.setAttribute("class", "demo");

    var headerInfo = document.createElement("h1");
    headerInfo.innerText = "What we do in this class:";
    infoDivElt.appendChild(headerInfo);

    var paragraphElt = document.createElement("p");
    paragraphElt.innerText = info;
    infoDivElt.appendChild(paragraphElt);
    
    var dynamicDiv = document.getElementById("dynamicContainer");
    dynamicDiv.appendChild(divElt);
    dynamicDiv.appendChild(infoDivElt);

    var buttonDiv = document.createElement("div");
    buttonDiv.setAttribute("style", "text-align:center;")

    var anchorElt = document.createElement("a");
    anchorElt.setAttribute("href", "timeslots.html");
    anchorElt.setAttribute("class", "button");
    anchorElt.innerText ="BOOK NOW";
    buttonDiv.appendChild(anchorElt);

    dynamicDiv.appendChild(buttonDiv);
    
}

renderVisibles();