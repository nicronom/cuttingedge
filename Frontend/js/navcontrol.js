const handleNavBar = () => {
    var nav = document.getElementsByClassName("topnav");

    // include the tabs which should always be there
    var mainTab = document.createElement("a");
    mainTab.setAttribute("class", "active");
    mainTab.setAttribute("href", "main.html");
    mainTab.innerText = "CUTTING EDGE";

    var classesTab = document.createElement("a");
    classesTab.setAttribute("href", "classes.html");
    classesTab.innerText = "Classes";

    var membershipsTab = document.createElement("a");
    membershipsTab.setAttribute("href", "membership.html");
    membershipsTab.innerText = "Memberships";

    console.log(nav);
    nav[0].appendChild(mainTab);
    nav[0].appendChild(classesTab);
    nav[0].appendChild(membershipsTab);

    if (sessionStorage.getItem("isLoggedIn") !== "true") {
        var logTab = document.createElement("a");
        logTab.setAttribute("href", "SignIn.html");
        logTab.innerText = "Log In";

        var registerTab = document.createElement("a");
        registerTab.setAttribute("href", "register.html");
        registerTab.innerText = "Register";

        nav[0].appendChild(logTab);
        nav[0].appendChild(registerTab);
    } else {
        var bookingsTab = document.createElement("a");
        bookingsTab.setAttribute("href", "mainprofile.html");
        bookingsTab.innerText = "My Bookings";

        nav[0].appendChild(bookingsTab);

        var signOut = document.createElement("a");
        signOut.onclick = () => {
            sessionStorage.setItem("isLoggedIn", "false");
            sessionStorage.setItem("userEmail", "");
            window.location.replace("main.html");
        }
        signOut.innerText = "Sign Out";

        nav[0].appendChild(signOut);
    }
}

handleNavBar();