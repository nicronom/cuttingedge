  const handleRegister = async(user) => {
    await createUser(user);

    if (sessionStorage.getItem("userEmail") !== "") {
      window.location.replace("mainprofile.html");
    }
  }

  const handleFailedRegister = () => {
    sessionStorage.setItem("userEmail", "");
    sessionStorage.setItem("isLoggedIn", "false");
    var message = document.getElementById("errMessage");
    message.setAttribute("style", "visibility: visible; color: red;");
  }
  
  const createUser = async(user) => {
      await axios.post('http://localhost:8080/users', user)
          .then(response => {
              sessionStorage.setItem("userEmail", user.email);
              sessionStorage.setItem("isLoggedIn", "true");
          })
          .catch(handleFailedRegister());

      return;
  };
  
  const form = document.querySelector('form');
  
  const formEvent = form.addEventListener('submit', event => {
      event.preventDefault();
     
    const email = document.querySelector('#email').value;
    const password = document.querySelector('#password').value;
    const firstName = document.querySelector('#firstName').value;

    const user = { email, password, firstName};
    handleRegister(user);
  });
  
  
  
  