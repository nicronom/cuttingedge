var actUrl = 'http://localhost:8080/activities';
var avUrl = 'http://localhost:8080/availability/byName?name=';

const fetchActivities = () => {
    axios({
        method: 'get',
        url: 'http://localhost:8080/activities',
        responseType: 'json'
    })
        .then(response => {
            const activities = response.data;
            console.log(`GET list activities`, activities);
        })
        .catch(error => console.error(error));
};

const addActivity = (activity, availability, availabilityUrl) => {
    axios.post(actUrl, activity)
        .then(response => {
            const addedActivity = response.data;
            console.log(`POST: activity is added`, addedActivity);
        })
        .catch(error => console.error(error));
    axios.post(availabilityUrl, availability)
    .then(response => {
        const addedActivity = response.data;
        console.log(`POST: availability is added`, addedActivity);
    })
};

const form = document.querySelector('form');

const formEvent = form.addEventListener('submit', event => {
    event.preventDefault();

    const actName = document.querySelector('#activityName').value;
    const fromH = document.querySelector('#fromHour').value;
    const untilH = document.querySelector('#untilHour').value;
    const availDay = document.querySelector('#availableDay').value;

    const availUrl = avUrl.concat(actName);

    const activity = {
        activityName : actName
    };
    const availability = {
      fromHour: fromH,
      untilHour: untilH,
      availableDay: availDay
    }
    addActivity(activity, availability, availUrl);
});

fetchActivities();
