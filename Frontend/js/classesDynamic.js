const fetchActivities = async() => {
    var activities;
    await axios({
        method: 'get',
        url: 'http://localhost:8080/activities',
        responseType: 'json'
    })
        .then(response => {
            activities = response.data;
        })
        .catch(error => console.error(error));

    return activities;
};

async function showActivities (fetchMethod) {
    var activities = await fetchMethod();
    var alts = ['profile-sample2', 'profile-sample7', 'profile-sample6'];
    var atAlt = 0;

    for (let i=0; i < activities.length; i++) {

        var activityFigure = document.createElement("figure");
        activityFigure.setAttribute("class", "sport");
        
        var activityImg = document.createElement("img");
        activityImg.setAttribute("src", activities[i].imgUrl);

        if (atAlt === 3) {
            atAlt = 0;
        }

        activityImg.setAttribute("alt", alts[atAlt]);
        atAlt++;

        activityFigure.appendChild(activityImg);

        var activityCaption = document.createElement("figcaption");
        var activityHeader = document.createElement("h3");
        activityHeader.setAttribute("class", "title1");
        activityHeader.innerText = activities[i].activityName;
        activityCaption.appendChild(activityHeader);
        activityFigure.appendChild(activityCaption);

        activityLink = document.createElement("a");
        activityLink.onclick = function() {
            sessionStorage.setItem("activityName", activities[i].activityName);
            sessionStorage.setItem("activityImg", activities[i].imgUrl);
            sessionStorage.setItem("activityInfo", activities[i].activityInfo);
            window.location.href = "specificClass.html";
        };
        activityFigure.appendChild(activityLink);

        var activitiesDiv = document.getElementById("activities");
        activitiesDiv.appendChild(activityFigure);
    }
}

function handleActivities() {
    showActivities(fetchActivities);
}

handleActivities();