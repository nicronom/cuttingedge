// gets the booked timeslots for the logged in user
const getUserBookings = async(reqUrl) => {
    var bookings;
    await axios({
        method: 'get',
        url: reqUrl,
        responseType: 'json'
    })
    .then(response => {
        bookings = response.data;
    })
    .catch(
        error => console.log(error)
    )

    return bookings;
}

// gets the name of the class of that timeslot
const getActivityName = async(availability) => {

    var activityName;

    await axios.get('http://localhost:8080/bookings/actName', 
    {
        params: {
            fromHour: availability.fromHour,
            untilHour: availability.untilHour,
            availableDay: availability.availableDay
        }
    })
    .then(response => {
        activityName = response.data;
    })
    .catch(
        error => console.log(error)
    )
    return activityName;
}

// gets if present the brief description of that activity
const getActivityInfo = async(availability) => {

    var activityInfo;

    await axios.get('http://localhost:8080/bookings/actInfo', 
    {
        params: {
            fromHour: availability.fromHour,
            untilHour: availability.untilHour,
            availableDay: availability.availableDay
        }
    })
    .then(response => {
        activityInfo = response.data;
    })
    .catch(
        error => console.log(error)
    )
    return activityInfo;
}

// "orders the bookings by days"
const swapBookingsOrder = (bookings) => {

    let mondayBookings = [];
    let tuesdayBookings = [];
    let wednesdayBookings = [];
    let thursdayBookings = [];
    let fridayBookings = [];
    let saturdayBookings = [];
    let sundayBookings = [];

    if ((bookings !== null) && (bookings !== undefined) && (bookings.length !== undefined)) {
        for (let i=0; i < bookings.length; i++) {
            if (bookings[i].availableDay === 'monday') {
                mondayBookings.push(bookings[i]);
            }
            if (bookings[i].availableDay === 'tuesday') {
                tuesdayBookings.push(bookings[i]);
            }
            if (bookings[i].availableDay === 'wednesday') {
                wednesdayBookings.push(bookings[i]);
            }
            if (bookings[i].availableDay === 'thursday') {
                thursdayBookings.push(bookings[i]);
            }
            if (bookings[i].availableDay === 'friday') {
                fridayBookings.push(bookings[i]);
            }
            if (bookings[i].availableDay === 'saturday') {
                saturdayBookings.push(bookings[i]);
            }
            if (bookings[i].availableDay === 'sunday') {
                sundayBookings.push(bookings[i]);
            }
        }
    } else {
        return null;
    }

    let orderedBookings = [];

    mondayBookings.forEach(elt => orderedBookings.push(elt));
    tuesdayBookings.forEach(elt => orderedBookings.push(elt));
    wednesdayBookings.forEach(elt => orderedBookings.push(elt));
    thursdayBookings.forEach(elt => orderedBookings.push(elt));
    fridayBookings.forEach(elt => orderedBookings.push(elt));
    saturdayBookings.forEach(elt => orderedBookings.push(elt));
    sundayBookings.forEach(elt => orderedBookings.push(elt));

    return orderedBookings;
}

// handles how bookings are displayed on the html page.
const showBookings = async(reqUrl) => {
    var bookings = await getUserBookings(reqUrl);
    bookings = swapBookingsOrder(bookings);

    if ((bookings !== null) && (bookings !== undefined) && (bookings.length !== undefined)){
        for (let i=0; i<bookings.length; i++) {

            var details = bookings[i];

            var activityName = await getActivityName(bookings[i]) + " ";

            var availableDay = details.availableDay;
            var fromHour = details.fromHour + "-";
            var untilHour = details.untilHour + " ";

            var activityInfo = await getActivityInfo(bookings[i]);
            console.log(activityInfo);

            var classElt = document.getElementById("timeline");

            var bookingElt = document.createElement("li");
            bookingElt.setAttribute("class", "event");
            bookingElt.setAttribute("data-date", fromHour + untilHour + availableDay);

            var hElt = document.createElement("h3");
            hElt.innerText = activityName;
            bookingElt.appendChild(hElt);

            if (activityInfo !== null && activityInfo !== undefined) {
                var pElt = document.createElement("p");
                pElt.innerText = activityInfo;
                bookingElt.appendChild(pElt);
            }
            
            classElt.appendChild(bookingElt);
        }
    } 
}

// handles the synchronisation of requests
async function handleBookings() {
    var defaultUrl = "http://localhost:8080/bookings/byEmail?email=";
    var requestUrl = defaultUrl.concat(sessionStorage.getItem("userEmail"));
    await showBookings(requestUrl);
}

handleBookings();