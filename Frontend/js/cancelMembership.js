const cancelMembership = async() => {
    var defaultUrl = "http://localhost:8080/users/sub?email=";
    var url = defaultUrl + sessionStorage.getItem("userEmail") + "&subvalue=false";
    sessionStorage.setItem("isSubscribed", "false");

    await axios.post(url).catch( function (error) {
        console.log(error);
    })
}

async function handleCancel() {
    await cancelMembership();
    window.location.replace("refund.html");
}