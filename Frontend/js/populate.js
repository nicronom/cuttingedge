// POPULATED ACTIVITIES: FITNESS AND BOXING
// POPULATED BOOKINGS: testUser1@abv.bg

// populating the server with data
const populate = async() => {
    var days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    var fromH = '14:00';
    var untilH = '16:00';
    var name = 'Swimming';

    // adds available times
    for (let i=0; i < days.length; i++) {
        
        // boxing is at different days and different times
        if (days[i] === 'thursday') {
            name = 'Boxing';
            fromH = '18:00';
            untilH = '21:00';
        }
        var availObj = {
            fromHour: fromH,
            untilHour: untilH,
            availableDay: days[i]
        }

        await addAvails(name, availObj);

        // book all sessions for a test user
        var bookingObj = {
            email: 'testUser1@abv.bg',
            availability: availObj
        }
        await addBookings(bookingObj);
    }

}

// populates the activity availabilities
const addAvails = async(name, availBody) => {

    var url = "http://localhost:8080/availability/byName?name=" + name;
    axios.post(url, availBody).catch(error => console.log(error));

}

// populates the bookings
const addBookings = async(bookingBody) => {
    var url = "http://localhost:8080/bookings/bookNew";
    axios.post(url, bookingBody).catch(error => console.log(error));
}

populate();

