
let amount = sessionStorage.getItem("amount");

let email = sessionStorage.getItem("userEmail");

var defaultUrl = 'http://localhost:8080/paymentReceipt/byEmail?email=';

var requestUrl = defaultUrl.concat(email);

const createPaymentReceipt = async(paymentReceipt) => {
    console.log(paymentReceipt);
    await axios.post(requestUrl,paymentReceipt)
        .then(response => console.log(response))
        .catch(error => console.log(error));

};

document.getElementById("payment").addEventListener('click', event => {
        event.preventDefault();

    const receiptRandom = Math.random().toString(36).substring(3);
    sessionStorage.setItem("random", receiptRandom);


        let  date = new Date().toISOString();

        const paymentReceipt = {
            amount: amount,
            dateIssued: date
        };

        createPaymentReceipt(paymentReceipt);
        window.location.replace("receipt.html");

    });
