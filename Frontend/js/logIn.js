var defaultUrl = 'http://localhost:8080/users/byEmail?email=';

const handleLoginFail = () => {
    var message = document.getElementById("errMessage");
    message.setAttribute("style", "color: red; visibility: visible;");
    sessionStorage.setItem("isLoggedIn", "false");
    sessionStorage.setItem("userEmail", "");
}

const getUserByEmail= (user) => {
    axios.get(user.url)
        .then(response => {
            var existingUser = response.data;
            if(existingUser.password === user.password) {
                console.log("Login completed!");
                sessionStorage.setItem("userEmail", user.email);
                sessionStorage.setItem("isLoggedIn", "true");
                window.location.replace("mainprofile.html");
            } else {
                handleLoginFail();
            }
        })
        .catch(handleLoginFail());

};

const form = document.querySelector('form');

const formEvent = form.addEventListener('submit', event => {
    event.preventDefault();

    const userEmail = document.querySelector('#email').value;
    const userPassword = document.querySelector('#password').value;
    const requestUrl = defaultUrl.concat(userEmail);

    const userObj = {
      email: userEmail,
      password: userPassword,
      url: requestUrl
    }
    getUserByEmail(userObj);

});

