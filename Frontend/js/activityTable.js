var actUrl = 'http://localhost:8080/activities';
var avUrl = 'http://localhost:8080/availability/byName?name=';

var actName = document.querySelector(".activity-name");
var fromH = document.querySelector(".fromHour");
var untilH = document.querySelector(".untilHour");
var availDay = document.querySelector(".availableDay");

const fetchActivities = async() => {
    var activities;
    await axios({
        method: 'get',
        url: 'http://localhost:8080/activities',
        responseType: 'json'
    })
        .then(response => {
            activities = response.data;
            console.log(`GET list activities`, activities);
        })
        .catch(error => console.error(error));

    return activities;
};

const showAvailability = async(activityName) => {
    await axios.get(avUrl + activityName)
      .then(response => {
        availabilities = response.data;

        for (let j=0; j < availabilities.length; j++) {
          var availElt = document.createElement("P");
          var availText = availabilities[j].fromHour + " " + availabilities[j].untilHour + " " + availabilities[j].availableDay;
          availElt.innerText = availText;
          document.body.appendChild(availElt);
        }

        console.log(`GET activity availabilities`, availabilities);
      })
      .catch(
        error => console.error(error)
      )
}

async function showActivities (fetchMethod) {
    var activities = await fetchMethod();
    console.log(activities);

    for (let i=0; i < activities.length; i++) {
        
      var activityNameElt = document.createElement("P");
      activityNameElt.innerText = activities[i].activityName;
      document.body.appendChild(activityNameElt);

      await showAvailability(activities[i].activityName);
    }

};

function handleActivities() {
  showActivities(fetchActivities);
}

handleActivities();
