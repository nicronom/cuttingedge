# CuttingEdge  
  
COMP2913 Software Engineering Project.  
  
## How to start the server:  
  
*  Check that you have maven installed by running "mvn --version" in the terminal.  
*  Navigate to the Api directory residing in the Backend directory of the project.  
*  Start the server by executing the following command in the terminal "mvn spring-boot:run".  
*  Wait until the initialising is finished.  
  
## User manual    
  
*  Classes and available timeslots can be viewed even if not signed in to the system.  
You can only book them however, if you have signed it to the system. Attempts to  
book a class timeslot, while not signed in will forward you to register. 
*  In order to book a class timeslot, you need to have an active subscription to the gym,  
therefore attempts to book without an active membership will result in your being forwarded to  
memberships section of the app.  
*  Payment for membership is simulated, therefore you can click on generate random card number  
button to simulate your payment for getting the respective membership.  
*  You are unable to buy another membership, until your current membership has expired.  
*  You can review your booked classes at any time navigating through the my bookings section of the navbar.  
*  You are able to request a refund to your payment of gym membership, if it was clicked by accident on spot.


