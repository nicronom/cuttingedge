package com.Restful.Api.users.users;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This class represents the primary logic, publically exposed as Api.
 * it has the responsibility to manage data access, consider edge cases and
 * indicate whether records are successfully extracted, i.e returns the
 * record itself or true if successful, null or false otherwise. */

@Service
public class UsersService {

    @Autowired
    private UsersRepository usersRepository;

    /* returns all current non-archived user instances held in the Users Table,
    * OR null if no such instances are found. */
    public List<Users> getAllUsers() {
        List<Users> users = new ArrayList<>();
        Iterable<Users> usersIterable = usersRepository.findAll();
        for (Users user: usersIterable) {
            if (!user.getArchived()) {
                users.add(user);
            }
        }

        if (users.size() != 0) {
            return users;
        } else {            // no records found
            return null;
        }
    }

    // attempts to retrieve a user instance by a given userId
    public Users getUserById(Integer id) {
        Optional<Users> userInstance = usersRepository.findById(id);

        // user instance with id exists and it is not being archived, i.e. considered removed
        if (userInstance.isPresent()
                && !userInstance.get().getArchived()) {
            return userInstance.get();
        } else {
            return null;
        }
    }

    // attempts to retrieve a user instance by a given user email
    public Users getUserByEmail(String email) {
        Users userRecord = null;
        Iterable<Users> users = usersRepository
                .findAll();
        for (Users user: users) {
            if (user.getEmail().equals(email) && !user.getArchived()) {
                userRecord = user;
                break;
            }

        }

        if (userRecord != null) {
            return userRecord;
        } else {
            return null;
        }
    }

    // attempts to add the user to the database
    public boolean addUser(Users user) {

        if (user == null) {
            return false;
        }

        // it is required that these json body parameters are set to some values
        if (user.getEmail() == null || user.getPassword() == null
                || user.getFirstName() == null) {
            return false;
        }

        // we need to check all user records even the archived ones
        Iterable<Users> users = usersRepository
                .findAll();
        for (Users userIter: users) {
            if (userIter.getEmail().equals(user.getEmail())) {
                return false;
            }
        }

        // if no user record with such user name exists, the record can be saved
        usersRepository.save(user);
        return true;
    }

    // changes the account administrator privileges of a given user.
    public boolean updateUserIsAdmin(Users user, boolean value) {
        if (user != null) {
            user.setIsAdmin(value);
            usersRepository.save(user);
            return true;
        } else {
            return false;
        }
    }

    // given a user to be removed, archives the instance in the Table instead.
    public boolean archiveUser(Users user) {
        if (user != null) {
            user.setArchived(true);
            usersRepository.save(user);
            return true;
        } else {
            return false;
        }
    }

    public boolean manageUserSubscription(Users user, boolean value) {
        if (user != null) {
            user.setSubscribed(value);
            usersRepository.save(user);
            return true;
        } else {
            return false;
        }
    }
}
