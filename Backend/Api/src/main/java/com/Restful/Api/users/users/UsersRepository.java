package com.Restful.Api.users.users;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/** Container class (repository) for the SQL User Table itself */

@Repository
public interface UsersRepository extends CrudRepository<Users,Integer>{

}
