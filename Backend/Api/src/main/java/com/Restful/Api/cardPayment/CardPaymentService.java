package com.Restful.Api.cardPayment;


import java.util.Date;


import com.Restful.Api.paymentReceipt.PaymentReceipt;
import com.Restful.Api.paymentReceipt.PaymentReceiptService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**All access to the card payment database records, including
 * insertion, deletion and alterations are defined in this class.
 * Card payment service class also uses composite field of
 * the payment receipt services to use its logic when manipulating the
 * respective database records. */

@Service
public class CardPaymentService {
    // in order to use methods to access payment receipt data
    private PaymentReceiptService paymentReceiptService;

    @Autowired
    private CardPaymentRepository cardPaymentRepository;

    // the field value is automatically set on paymentReceiptService creation
    @Autowired
    public void setPaymentReceiptService(PaymentReceiptService paymentReceiptService) {
        this.paymentReceiptService = paymentReceiptService;
    }

    /* attempts to retrieve a cardPayment instance by a given user email, purchase amount and date
     * Returns the object if successful, null otherwise.
     */
    public CardPayment getCardPayment(String email, String amount, Date date) {

        CardPayment cardPaymentRecord = null;
        // attempt to match a card payment for that payment receipt
        Iterable<CardPayment> cardPayments = cardPaymentRepository
                .findAll();

        for (CardPayment cardPayment : cardPayments) {
            // when the card payment is deleted it should not be displayed.
            if (cardPayment.getPaymentReceipt().getUser().getEmail().equals(email) &&
                    cardPayment.getPaymentReceipt().getDateIssued().compareTo(date) == 0 &&
                    cardPayment.getPaymentReceipt().getAmount().equals(amount) &&
                    !cardPayment.getArchived()) {
                cardPaymentRecord = cardPayment;
                break;
            }
        }
        // returns null if unsuccessful to retrieve card details
        return cardPaymentRecord;
    }

    // attempts to add the cardPayment to the database
    public boolean addCardPayment(String email, CardPayment cardPayment) {

        // check payment receipt exists
        PaymentReceipt paymentReceipt = paymentReceiptService.getLatestPaymentReceipt(email);
        if (paymentReceipt == null) {
            return false;
        }

        // checks if a valid card Payment body is supplied
        if (cardPayment == null || cardPayment.getCardNumber() == null ) {
            return false;
        }

        // if no payment record with such paymentId exists, the record can be saved
        cardPaymentRepository.save(cardPayment);
        return true;
    }

    // given a to be removed, archives the instance in the Table instead.
    public boolean archiveCardPayment(CardPayment cardPayment) {
        if (cardPayment != null) {
            cardPayment.setArchived(true);
            cardPaymentRepository.save(cardPayment);
            return true;
        } else {
            return false;
        }
    }
}
