package com.Restful.Api.ActivityAvailability;

import com.Restful.Api.Activity.Activity;
import com.Restful.Api.Activity.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/** The logic of all data access to activity availabilities is provided below*/
@Service
public class AvailabilityService {
    //creating service in order to activity data
    private ActivityService activityService;

    @Autowired
    private AvailabilityRepository availabilityRepository;

    // the field value is automatically set on activityService creation
    @Autowired
    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }

    // attempts to get all current availabilities for an activity.
    public List<Availability> getActivityAvailability(String activityName) {

        // the activity that matches the name specified
        Activity activity = activityService.getActivityByName(activityName);

        if (activity == null) {
            return null;
        }
        // the list holding the availability records found for that activity name
        List<Availability> availabilityList = new ArrayList<>();

        // if a record's activity name matches, adds the record to the list
        Iterable<Availability> availabilities = availabilityRepository.findAll();
        for (Availability availability : availabilities) {
            if (availability.getActivity().getActivityName().equals(activityName)) {
                availabilityList.add(availability);
            }
        }

        if (availabilityList.size() == 0) {
            return null;
        }

        return availabilityList;
    }

    public Availability getSingleAvailability(Availability availability) {
        if (availability == null
            || availability.getFromHour() == null
            || availability.getUntilHour() == null
            || availability.getAvailableDay() == null) {
            return null;
        }

        Iterable<Availability> allAvails = availabilityRepository.findAll();
        for (Availability avail: allAvails) {
            if(avail.equals(availability)) {
              return avail;
            }
        }

        return null;
    }

    // attempts to add a new availability to a given activity
    public boolean addActivityAvailability(String activityName,
                                           Availability availability) {
        Activity activity = activityService.getActivityByName(activityName);

        // checks if there is a matching activity with that name
        if (activity == null) {
            return false;
        }

        // checks if a valid availability body is provided
        if (availability == null
                || availability.getFromHour() == null
                || availability.getUntilHour() == null
                || availability.getAvailableDay() == null) {
            return false;
        }

        String theDay = availability.getAvailableDay().toLowerCase();
        // check if available day is a day of the week
        if (!(theDay.equals("monday")
                || theDay.equals("tuesday")
                || theDay.equals("wednesday")
                || theDay.equals("thursday")
                || theDay.equals("friday")
                || theDay.equals("saturday")
                || theDay.equals("sunday"))) {
            return false;
        }

        // check if an activity is already available at that time in the db records
        Iterable<Availability> availabilities = availabilityRepository.findAll();
        for (Availability availRecord: availabilities) {
            if ( (availRecord.getAvailableDay().equals(availability.getAvailableDay()))
                    && (availRecord.getFromHour().equals(availability.getFromHour())
                        || availRecord.getUntilHour().equals(availability.getUntilHour())
                        )
                ) {
                return false;
            }
        }

        // all checks have passed, an availability can now be safely added
        availability.setActivity(activity);
        // handles the referencing
        activity.addAvailability(availability);
        // saves the new record
        availabilityRepository.save(availability);
        return true;
    }

    // deletes the matching availability of a given record with that activity name
    public boolean deleteActivityAvailability(String activityName,
                                              Availability availability) {

        // we are only interested in the availabilities of the matcing activity
        List<Availability> currAvails = getActivityAvailability(activityName);
        // the activity matching to the parameter name provided
        Activity requestActivity = activityService.getActivityByName(activityName);
        availability.setActivity(requestActivity);
        // parse through all availabilities of that activity looking for a match
        for (Availability avail : currAvails) {
            if (avail.equals(availability)) {
                availabilityRepository.delete(avail);
                return true;
            }
        }

        return false;
    }
}




