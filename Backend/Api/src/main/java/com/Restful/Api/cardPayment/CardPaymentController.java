package com.Restful.Api.cardPayment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.Date;

/** The Api request endpoints for card payment access for payment receipts are defined below*/
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CardPaymentController {

    @Autowired
    public CardPaymentService cardPaymentService;


    // Endpoint to retrieve a card payment of a given payment receipt cardPayment, by a given email, amount and date.
    @RequestMapping(method = RequestMethod.GET, value = "/cardPayment{email}{amount}{date}")
    @ResponseBody
    public CardPayment getCardPayment(@RequestParam String email,
                                      @RequestParam String amount,
                                      @RequestParam Date date) {

        CardPayment cardPayment = cardPaymentService.getCardPayment(email, amount, date);
        if (cardPayment != null) {
            return cardPayment;
        } else {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "cardPayment not found"
            );
        }
    }


    // attempts to save a new paymentCard record in the DB
    @RequestMapping(method=RequestMethod.POST, value="/cardPayment")
    public void addCardPayment(@RequestBody String email,
                               @RequestBody CardPayment cardPayment) {
        // first checks whether the body has all requires field values

        boolean isSuccess = cardPaymentService.addCardPayment(email,cardPayment);
        if (!isSuccess) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "invalid cardPayment format or cardPayment already exists"
            );
        }
    }

    // attempts to archive a CardPayment
    @RequestMapping(method=RequestMethod.DELETE, value="/cardPayment/removeCardPayment{email}{amount}{date}")
    public void deleteCardPayment(@RequestParam String email,
                                  @RequestParam String amount,
                                  @RequestParam Date date) {

        CardPayment cardPayment = cardPaymentService.getCardPayment(email, amount, date);
        if ( cardPayment == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "cardPayment not found"
            );
        }

        boolean isSuccess = cardPaymentService.archiveCardPayment(cardPayment);
        if (!isSuccess) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST
            );
        }
    }

}
