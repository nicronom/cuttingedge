package com.Restful.Api.paymentReceipt;
import java.util.ArrayList;
import java.util.List;

import com.Restful.Api.users.users.Users;
import com.Restful.Api.users.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This class represents the primary logic, publically exposed as Api.
 * it has the responsibility to manage data access, consider edge cases and
 * indicate whether records are successfully extracted, i.e returns the
 * record itself or true if successful, null or false otherwise. */


@Service
public class PaymentReceiptService {
    private UsersService usersServices;

    @Autowired
    private PaymentReceiptRepository paymentReceiptRepository;

    @Autowired
    public void setUsersServices(UsersService usersServices) { this.usersServices = usersServices; }

    /* returns all payment receipts held in the PaymentReceipt Table for that user (given the user's email),
     * OR null if no such instances are found. */
    public List<PaymentReceipt> getAllPaymentReceipt(String email) {
        Users user = usersServices.getUserByEmail(email);
        if (user == null) {
            return null;
        }

        List<PaymentReceipt> receipts = new ArrayList<>();
        Iterable<PaymentReceipt> receiptsIterable = paymentReceiptRepository.findAll();
        for (PaymentReceipt receipt: receiptsIterable) {
            if (receipt.getUser().getEmail().equals(email)) {
                receipts.add(receipt);
            }
        }

        if (receipts.size() > 0) {
            return receipts;
        } else {
            // no records found
            return null;
        }
    }
    // attempts to retrieve the latest payment receipt instance for a user by a given email
    public PaymentReceipt getLatestPaymentReceipt(String email){
        List<PaymentReceipt> paymentReceipts = getAllPaymentReceipt(email);

        int highestId = 0;
        PaymentReceipt paymentReceipt = null;

        if (paymentReceipts.size() !=  0) {
            for (PaymentReceipt receipt: paymentReceipts) {
                if (receipt.getReceiptId() > highestId) {
                    //Update the highest id.
                    highestId = receipt.getReceiptId();
                    paymentReceipt = receipt;
                    }
                }
        }
        return paymentReceipt;
    }


    // attempts to add the paymentReceipt to the database
    public boolean addPaymentReceipt(String email, PaymentReceipt receipt) {
        Users user = usersServices.getUserByEmail(email);
        if (user == null) {
            return false;
        }

        if (receipt == null) {
            return false;
        }

        // it is required that these json body parameters are set to some values
        if (receipt.getAmount() == null || receipt.getDateIssued() == null) {
            return false;
        }

        receipt.setUser(user);
        List<PaymentReceipt> payments = user.getPaymentReceipts();
        payments.add(receipt);
        paymentReceiptRepository.save(receipt);
        return true;
    }

}
