package com.Restful.Api.ActivityAvailability;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/** The Api endpoints for activity availabilities are exposed below. */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AvailabilityController {
    @Autowired
    private AvailabilityService availabilityService;

    // The endpoint to get all available times for a given activity name
    @RequestMapping(method = RequestMethod.GET,value = "/availability/byName{name}")
    @ResponseBody
    public List<Availability> getActivityAvailability(@RequestParam String name) {
        List<Availability> avails = availabilityService.getActivityAvailability(name);
        if (avails != null) {
            return avails;
        } else {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "No availabilities for " + name + " found."
            );
        }
    }

    // The endpoint to create a new record of activity availability by provided name
    @RequestMapping(method = RequestMethod.POST, value = "/availability/byName{name}")
    public void addActivityAvailability(@RequestParam String name, @RequestBody Availability avail
                                        ) {
        boolean isSuccess = availabilityService.addActivityAvailability(name, avail);
        if (!isSuccess) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Invalid activity name or availability."
            );
        }
    }

    // the endpoint to remove the availability record by a given activity name
    @RequestMapping(method=RequestMethod.DELETE, value="/availability/byName{name}")
    public void deleteAvailability(@RequestBody Availability avail, @RequestParam String name) {
        boolean isSuccess = availabilityService.deleteActivityAvailability(name, avail);
        if (!isSuccess) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST
            );
        }
    }
}
