package com.Restful.Api.Booking;

import com.Restful.Api.ActivityAvailability.Availability;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * The REST API controller endpoints for the booking
 * data access is defined as follows. */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BookingController {
    @Autowired
    private BookingService bookingService;

    @RequestMapping(method = RequestMethod.GET, value = "/bookings")
    @ResponseBody
    public List<Booking> getAvailabilityBookings(@RequestBody Availability availability) {
        List<Booking> bookings = bookingService.getBookings(availability);
        if (bookings != null) {
            return bookings;
        } else {
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "No bookings found for this timeslot."
            );
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/bookings/byEmail")
    @ResponseBody
    public List<Availability> getUserBookings(@RequestParam String email) {
        List<Availability> bookings = bookingService.getUserBookings(email);
        if (bookings != null) {
            return bookings;
        } else {
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "No bookings found for email " + email
            );
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/bookings/actName")
    @ResponseBody
    public String getAvailActivityName(@RequestParam String fromHour,
                                       @RequestParam String untilHour,
                                       @RequestParam String availableDay) {
        Availability tempAvail = new Availability(fromHour, untilHour, availableDay);
        String name = bookingService.getAvailName(tempAvail);
        if (name != null) {
            return name;
        } else {
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND
            );
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/bookings/actInfo")
    @ResponseBody
    public String getActivityInfo(@RequestParam String fromHour,
                                       @RequestParam String untilHour,
                                       @RequestParam String availableDay) {
        Availability tempAvail = new Availability(fromHour, untilHour, availableDay);
        String info = bookingService.getActivityInfo(tempAvail);
        if (info != null) {
            return info;
        } else {
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND
            );
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/bookings/bookNew")
    public void addNewBooking(@RequestBody Booking booking) {
        boolean isSuccess = bookingService.addBooking(booking);
        if (!isSuccess) {
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST
            );
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/bookings/removeBook")
    public void removeBook(@RequestParam String fromHour,
                           @RequestParam String untilHour,
                           @RequestParam String availableDay,
                           @RequestParam String email) {
        Availability tempAvail = new Availability(fromHour, untilHour, availableDay);
        Booking theBooking = new Booking(email, tempAvail);
        boolean isSuccess = bookingService.deleteBooking(theBooking);
        if (!isSuccess) {
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST
            );
        }
    }
}
