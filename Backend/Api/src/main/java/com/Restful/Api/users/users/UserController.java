package com.Restful.Api.users.users;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 * The data access communication protocol through HTTP requests are
 * defined in this controller class. Uses standard and descriptive
 * RESTful Api naming conventions for GET/POST/PUT/DELETE requests.
 * Given the interface of communication with the service logic implementation,
 * the component knows when a request has been unsuccessful, and
 * handles such queries by indicating an appropriate server response.
 * When successful returns OK and possibly the JSON representation of the
 * object itself. */

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

    @Autowired
    public UsersService usersService;

    // attempts to get information for all user instances from the database
    @RequestMapping("/users")
    public List<Users> getUsers() {

        List<Users> users = usersService.getAllUsers();
        // return list of all users if any user instances are present in the DB
        if (users != null) {
            return users;
        } else {            // otherwise throw 404, indicating that a user was not found
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "users not found"
            );
        }
    }

    // attempts to get information on a single instance of a user, by a given email
    @RequestMapping(method = RequestMethod.GET, value = "/users/byEmail{email}")
    @ResponseBody
    public Users getUser(@RequestParam String email) {
        Users user = usersService.getUserByEmail(email);
        if(user != null) {
            return user;
        } else {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "user not found"
            );
        }
    }

    // attempts to save a new user record in the DB
    @RequestMapping(method=RequestMethod.POST, value="/users")
    public void addUser(@RequestBody Users user) {
        // first checks whether the body has all requires field values

        boolean isSuccess = usersService.addUser(user);
        if (!isSuccess) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "invalid user format or user already exists"
            );
        }
    }

    // attempts to set the administrator tag for a user instance if present, given an email
    @RequestMapping(method=RequestMethod.PUT, value="/users/setAdmin{email}{isAdmin}")
    public void updateUserIsAdmin(@RequestParam String email,
                                  @RequestParam boolean isAdmin) {
        Users user = usersService.getUserByEmail(email);
        // user might not exist
        if (user == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "user not found"
            );
        }

        boolean isSuccess = usersService.updateUserIsAdmin(user, isAdmin);
        // query might be unsuccessful even if a user exists
        if (!isSuccess) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST
            );
        }
    }

    // attempts to archive a user, given the user's email
    @RequestMapping(method=RequestMethod.DELETE, value="/users/removeUser{email}")
    public void deleteUser(@RequestParam String email) {
        Users user = usersService.getUserByEmail(email);
        if (user == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "user not found"
            );
        }

        boolean isSuccess = usersService.archiveUser(user);
        if (!isSuccess) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST
            );
        }
    }

    @RequestMapping(method=RequestMethod.POST, value="/users/sub{email}{subvalue}")
    public void handleUserSubscribe(@RequestParam String email, @RequestParam boolean subvalue) {
        Users user = usersService.getUserByEmail(email);
        if (user == null) {
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "user not found"
                );
        }

        boolean isSuccess = usersService.manageUserSubscription(user, subvalue);
        if (!isSuccess) {
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST
            );
        }
    }

}
