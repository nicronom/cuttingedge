package com.Restful.Api.paymentReceipt;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/*Repository for the PaymentReceipt SQL Table */

@Repository
public interface PaymentReceiptRepository extends CrudRepository<PaymentReceipt,Integer>{

}
