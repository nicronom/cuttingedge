package com.Restful.Api.cardDetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/** The Api request endpoints for card details access for users are defined below*/
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CardDetailsController {
    @Autowired
    private CardDetailsService cardDetailsService;

    // Endpoint to retrieve a card detail of a given user
    @RequestMapping(method=RequestMethod.GET, value="/cardDetails/byEmail{email}")
    public CardDetails getCardDetails(@RequestParam String email) {
        CardDetails details = cardDetailsService.getCardDetails(email);
        if (details != null) {
            return details;
        } else {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Card details for " + email + " do not exist."
            );
        }
    }

    // Endpoint to create a record of user's card details
    @RequestMapping(method=RequestMethod.POST, value="/cardDetails/byEmail{email}")
    public void addDetails(@RequestBody CardDetails details, @RequestParam String email) {
        // first checks whether the body has all requires field values
        boolean isSuccess = cardDetailsService.addCardDetails(email, details);
        if (!isSuccess) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "invalid card format or card already exists"
            );
        }
    }

    // endpoint to update the card details of the user
    @RequestMapping(method=RequestMethod.PUT, value="/cardDetails/updateDetails{email}")
    public void updateCardDetails(@RequestBody CardDetails details, @RequestParam String email) {
        boolean isSuccess = cardDetailsService.updateCardDetails(email, details);
        if (!isSuccess) {
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST);
        }
    }

    // endpoint to delete, e.g. archive the card detail record of the user.
    @RequestMapping(method=RequestMethod.DELETE, value="/cardDetails/byEmail{email}")
    public void deleteCardDetails(@RequestParam String email) {
        boolean isSuccess = cardDetailsService.archiveDetails(email);
        if (!isSuccess) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST
            );
        }
    }
}
