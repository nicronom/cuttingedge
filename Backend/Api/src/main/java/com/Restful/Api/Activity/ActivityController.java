package com.Restful.Api.Activity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * The data access communication protocol through HTTP requests are
 * defined in this controller class. Uses standard and descriptive
 * RESTful Api naming conventions for GET/POST/PUT/DELETE requests.
 * Given the interface of communication with the service logic implementation,
 * the component knows when a request has been unsuccessful, and
 * handles such queries by indicating an appropriate server response.
 * When successful returns OK and possibly the JSON representation of the
 * object itself. */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ActivityController {

    @Autowired
    public ActivityService activityService;

    //gets information for all the activities into the database
    @RequestMapping("/activities")
    @ResponseBody
    public List<Activity> getActivities() {

        List<Activity> activities = activityService.getAllActivities();
        //return a list of all the activities
        if (activities != null){
            return activities;
        } else{ // if there are no activities , throw 404, saying that there are no activities found
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,"activities not found"
            );
        }
    }

    //gets information for a single activity by a given name.
    @RequestMapping(method = RequestMethod.GET, value ="/activities/byName{name}")
    @ResponseBody
    public Activity getActivity(@RequestParam String name){
        Activity activity = activityService.getActivityByName(name);
        if(activity != null){
            return activity;
        }else{
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,"activity not found!"
            );
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/activities")
    public void addActivity(@RequestBody Activity activity) {
        boolean isSuccess = activityService.addActivity(activity);
        if(!isSuccess) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Invalid activity or activity already exists"
            );
        }
    }
        // archives an activity , given by its name
    @RequestMapping(method = RequestMethod.DELETE,value = "/activities/removeActivity{name}")
    public void archiveActivity(@RequestParam String name){
        Activity activity = activityService.getActivityByName(name);
        if(activity == null){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "activity not found"
            );
        }
        boolean isSuccess = activityService.archiveActivity(activity);
        if(!isSuccess){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST
            );
        }
    }

}
