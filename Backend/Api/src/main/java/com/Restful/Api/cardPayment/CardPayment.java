package com.Restful.Api.cardPayment;

import javax.persistence.*;

import com.Restful.Api.paymentReceipt.PaymentReceipt;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**Definition of card payment schema is provided in this class.
 * Each card payment has an automatically generated primary key on
 * creation, as well as one to one association with its respective payment receipt.
 * All card payment have to be provided in the appropriate format on creation.*/

@Entity
public class CardPayment {

    //Generating an incremental Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int paymentId;

    // each card payment is associated with a payment receipt
    @JsonBackReference
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "paymentReceipt_id")
    private PaymentReceipt paymentReceipt;


    @Column(nullable = false)
    private String cardNumber;

    @Column(columnDefinition = "boolean default false")
    private boolean archived;


    public CardPayment(String cardNumber) {
        this.cardNumber = cardNumber;
        this.archived = false;
    }

    public CardPayment(String cardNumber, boolean archived) {
        this.cardNumber = cardNumber;
        this.archived = archived;
    }

    public CardPayment() {
    }

    public int getPaymentId() {
        return this.paymentId;
    }

    public String getCardNumber() {
        return this.cardNumber;
    }

    public boolean getArchived() {
        return this.archived;
    }

    public void setArchived(boolean value) {
        this.archived = value;
    }

    public PaymentReceipt getPaymentReceipt() {
        return this.paymentReceipt;
    }

    public void setPaymentReceipt(PaymentReceipt paymentReceipt) {
        this.paymentReceipt = paymentReceipt;
    }
}