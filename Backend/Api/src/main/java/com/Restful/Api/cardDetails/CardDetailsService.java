package com.Restful.Api.cardDetails;

import com.Restful.Api.users.users.Users;
import com.Restful.Api.users.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**All access to the card details database records, including
 * insertion, deletion and alterations are defined in this class.
 * Card details service class also uses composite field of
 * the user services to use its logic when manipulating the
 * respective database records. */
@Service
public class CardDetailsService {
    // in order to use methods to access user data
    private UsersService usersService;

    @Autowired
    private CardDetailsRepository cardDetailsRepository;

    // the field value is automatically set on userService creation
    @Autowired
    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    // given a user email, attempts to find the record of his/her
    // card details. Returns the object if successful, null otherwise.
    public CardDetails getCardDetails(String email) {
        // first check if a user with that email exists
        Users user = usersService.getUserByEmail(email);
        if (user == null) {
            return null;
        }

        // then attempt to match a card detail for that user
        Iterable<CardDetails> allDetails = cardDetailsRepository
                .findAll();
        for (CardDetails details : allDetails) {
            // when the card detail is deleted it should not be displayed.
            if (details.getUser().getEmail().equals(email)
                    && !details.isArchived()) {
                return details;
            }
        }

        // unsuccessful to retrieve card details
        return null;
    }

    // Given a user email and card details object, attempts to add
    // a new record to the database. Unsuccessful if a user with that email
    // is not found, the user already has a card associated with him/her, i.e.
    // user's card details should either be non-existing or archived.
    public boolean addCardDetails(String email, CardDetails details) {
        // checks if a user exists
        Users user = usersService.getUserByEmail(email);
        if (user == null) {
            return false;
        }

        // checks if a valid card detail body is supplied
        if (details == null || details.getCardHolderName() == null
            || details.getCardNumber() == null || details.getCvv() == null
            || details.getExpiryDate() == null) {
            return false;
        }

        // checks if a record of card details for that user already exists
        Iterable<CardDetails> allDetails = cardDetailsRepository
                .findAll();
        for (CardDetails existingDetails : allDetails) {
            if (existingDetails.getUser().getEmail().equals(email)) {
                // details need to be updated if they're archived
                if (!existingDetails.getUser().getArchived()) {
                    return updateCardDetails(email, details);
                }
                return false;
            }
        }

        // if all checks passed, then the record can be safely saved
        // set the card details of the user
        user.setCardDetails(details);
        // and set the user of the card detail, to maintain the association
        details.setUser(user);
        cardDetailsRepository.save(details);
        return true;
    }

    // attempts to update the card detail information from a given user email.
    // This can be performed only if the user and respective card detail of that
    // user exist.
    public boolean updateCardDetails(String email, CardDetails details) {
        Users user = usersService.getUserByEmail(email);
        if (user == null) {
            return false;
        }

        if (details == null || details.getCardHolderName() == null
                || details.getCardNumber() == null || details.getCvv() == null
                || details.getExpiryDate() == null) {
            return false;
        }

        // check for an email match of a card detail record in the db
        Iterable<CardDetails> allDetails = cardDetailsRepository.findAll();
        for (CardDetails existingDetails: allDetails) {
            if (existingDetails.getUser().getEmail().equals(email)) {
                // if card detail of the user was deleted before, then
                // archived needs to be updated
                if (!existingDetails.getUser().getArchived()) {
                    existingDetails.setArchived(false);
                }
                // updates the appropriate card detail information from the supplied body
                existingDetails.setCardHolderName(details.getCardHolderName());
                existingDetails.setCardNumber(details.getCardNumber());
                existingDetails.setCvv(details.getCvv());
                existingDetails.setExpiryDate(details.getExpiryDate());
                user.setCardDetails(existingDetails);
                cardDetailsRepository.save(existingDetails);
                return true;
            }
        }

        return false;
    }

    // Attempts to archive the card detail information, given a user email
    public boolean archiveDetails(String email) {
        // attempts to find the card detail record of that email
        CardDetails cardDetails = getCardDetails(email);
        // if successful, then archives the record
        if (cardDetails != null) {
            cardDetails.setArchived(true);
            cardDetailsRepository.save(cardDetails);
            return true;
        } else {
            return false;
        }
    }
}
