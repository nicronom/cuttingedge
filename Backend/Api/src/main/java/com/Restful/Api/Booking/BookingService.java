package com.Restful.Api.Booking;

import com.Restful.Api.ActivityAvailability.Availability;
import com.Restful.Api.ActivityAvailability.AvailabilityRepository;
import com.Restful.Api.ActivityAvailability.AvailabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/** The implementation logic of the data access layer for bookings. */
@Service
public class BookingService {

    private AvailabilityService availabilityService;

    // both booking and availability repos are modified,
    // these are wired below
    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private AvailabilityRepository availabilityRepository;

    @Autowired
    public void setAvailabilityService(AvailabilityService availabilityService) {
        this.availabilityService = availabilityService;
    }

    // for a certain activity availability, gets all the current booked sessions
    public List<Booking> getBookings(Availability availability) {
        Availability theAvail = availabilityService.getSingleAvailability(availability);
        if (theAvail == null) {
            return null;
        }

        if (theAvail.getBookings() == null || theAvail.getBookings().isEmpty()) {
            return null;
        }

        return theAvail.getBookings();
    }

    public List<Availability> getUserBookings(String email) {
        if (email == null) {
            return null;
        }

        List<Availability> bookedSessions = new LinkedList<>();
        Iterable<Availability> allAvails = availabilityRepository.findAll();
        for (Availability avail : allAvails) {
            Iterable<Booking> availBookings = avail.getBookings();
            for (Booking book: availBookings) {
                if (book.getEmail().equals(email)) {
                    bookedSessions.add(avail);
                }
            }
        }

        if (!bookedSessions.isEmpty()) {
            return bookedSessions;
        }
        else {
            return null;
        }
    }

    public String getAvailName(Availability availability) {
        Availability avail = this.availabilityService.getSingleAvailability(availability);
        if (avail == null) {
            return null;
        }
        return avail.getActivity().getActivityName();
    }

    public String getActivityInfo(Availability availability) {
        Availability avail = this.availabilityService.getSingleAvailability(availability);
        if (avail == null) {
            return null;
        }
        return avail.getActivity().getActivityInfo();
    }

    // adds a user to a given available class
    public boolean addBooking(Booking booking) {
        if (booking == null || booking.getEmail() == null || booking.getAvailability() == null) {
            return false;
        }

        Availability theAvail = this.availabilityService.getSingleAvailability(booking.getAvailability());
        if (theAvail != null) {
            List<Booking> bookings = getBookings(theAvail);

            if (bookings != null) {
                for (Booking book : bookings) {
                    if (book.getEmail().equals(booking.getEmail())) {
                        return false;
                    }
                }
            }

            booking.setAvailability(theAvail);
            theAvail.addBooking(booking);
            bookingRepository.save(booking);
            return true;
        }

        return false;
    }

    // removes a user from a given available class.
    public boolean deleteBooking(Booking booking) {
        if (booking == null || booking.getEmail() == null || booking.getAvailability() == null) {
            return false;
        }

        List<Booking> allBookings = getBookings(booking.getAvailability());
        if (allBookings == null) {
            return false;
        }

        for (Booking theBooking : allBookings) {
            if (theBooking.getEmail().equals(booking.getEmail())
                && theBooking.getAvailability().equals(booking.getAvailability())) {
                Availability theAvail = theBooking.getAvailability();
                theAvail.removeBooking(theBooking);
                availabilityRepository.save(theAvail);
                bookingRepository.delete(theBooking);
                return true;
            }
        }

        return false;
    }
}
