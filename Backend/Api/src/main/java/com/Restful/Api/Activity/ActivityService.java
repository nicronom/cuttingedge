package com.Restful.Api.Activity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the primary logic, publically exposed as Api.
 * it has the responsibility to manage data access, consider edge cases and
 * indicate whether records are successfully extracted, i.e returns the
 * record itself or true if successful, null or false otherwise. */
@Service
public class ActivityService {

    @Autowired
    private  ActivityRepository activityRepository;

    /* returns all current non-archived user instances held in the Users Table,
     * OR null if no such instances are found. */
     public List<Activity> getAllActivities(){

         List<Activity> activities = new ArrayList<>();
         Iterable<Activity> activitiesIterable = activityRepository.findAll();
         for ( Activity activity: activitiesIterable) {
             if(!activity.getArchived()) {
                 activities.add(activity);
             }
         }

         if(activities.size() !=0) {
             return activities;
         } else{             // no records found
             return null;
         }

     }
        //attempts to retrieve an activity instance by given an activity name
     public Activity getActivityByName (String name ) {
         Activity activityRecord = null;
         Iterable<Activity> activities = activityRepository.findAll();
         for ( Activity activity : activities){
           if(activity.getActivityName().equals(name) && !activity.getArchived()) {
               activityRecord = activity;
               break;
           }
         }

         if(activityRecord != null) {
             return activityRecord;
         } else{
             return null;
         }
     }
     

    //Adding a new activity
    public boolean addActivity( Activity activity){
        if(activity == null) {
            return false;
        }
        // you have to input a name
        if(activity.getActivityName() == null){
            return false;
        }

        Iterable<Activity> activities = activityRepository.findAll();
        for (Activity activityIter:activities){
            //if the newly added activity name is already added , the system returns false
            if(activityIter.getActivityName().equals(activity.getActivityName())){
                return false;
            }
        }
        activityRepository.save(activity);
        return true;
    }
    // if the activity is no longer in the list , it  is not removed from the db , but just archived, and
    // it can be returned in a lated point.
    public boolean archiveActivity( Activity activity ) {
         if (activity != null){
             activity.setArchived(true);
             activityRepository.save(activity);
             return true;
         }else{
             return false;
         }
    }

}
