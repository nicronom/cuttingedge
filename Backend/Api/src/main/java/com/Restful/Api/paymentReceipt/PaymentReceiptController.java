package com.Restful.Api.paymentReceipt;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 * The data access communication protocol through HTTP requests are
 * defined in this controller class. Uses standard and descriptive
 * RESTful Api naming conventions for GET/POST/PUT/DELETE requests.
 * Given the interface of communication with the service logic implementation,
 * the component knows when a request has been unsuccessful, and
 * handles such queries by indicating an appropriate server response.
 * When successful returns OK and possibly the JSON representation of the
 * object itself. */

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PaymentReceiptController {

    @Autowired
    public PaymentReceiptService paymentReceiptService;

    // attempts to get all payment receipts instances for a user, from the database by a given email
    @RequestMapping(method = RequestMethod.GET, value = "/paymentReceipt/byEmail{email}")
    @ResponseBody
    public List<PaymentReceipt> getPaymentReceipt(@RequestParam String email) {

        List<PaymentReceipt> paymentReceipt = paymentReceiptService.getAllPaymentReceipt(email);

        // return list of all payment receipts if any user instances are present in the DB
        if (paymentReceipt != null) {
            return paymentReceipt;
        } else {
            // otherwise throw 404, indicating that a user was not found
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "no payment receipts found"
            );
        }
    }

    // attempts to get information of the latest payment receipt for a user, by a given email
    @RequestMapping(method = RequestMethod.GET, value = "/latestPaymentReceipt/byEmail{email}")
    @ResponseBody
    public PaymentReceipt getLatestPaymentReceipt(@RequestParam String email) {

        PaymentReceipt paymentReceipt = paymentReceiptService.getLatestPaymentReceipt(email);

        if (paymentReceipt != null) {
            return paymentReceipt;
        }
        else {

            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "no payment receipts found"
            );
        }
    }

    // attempts to save a new paymentReceipt record in the DB
    @RequestMapping(method = RequestMethod.POST, value = "/paymentReceipt/byEmail{email}")
    public void addPaymentReceipt(@RequestBody PaymentReceipt paymentReceipt, @RequestParam String email) {

        // first checks whether the body has all required field values
        boolean isSuccess = paymentReceiptService.addPaymentReceipt(email, paymentReceipt);
        if (!isSuccess) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "invalid payment receipt format"
            );
        }
    }



}
