package com.Restful.Api.cardDetails;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/** The repository containing the card details repository is defined below.*/
@Repository
public interface CardDetailsRepository extends CrudRepository<CardDetails,Integer> {
}
