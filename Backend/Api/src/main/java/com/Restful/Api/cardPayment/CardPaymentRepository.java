package com.Restful.Api.cardPayment;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/*Repository for the card payment SQL Table */
@Repository
public interface CardPaymentRepository extends CrudRepository<CardPayment,Integer> {
}
