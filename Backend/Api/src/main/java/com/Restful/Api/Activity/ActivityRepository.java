package com.Restful.Api.Activity;

import com.Restful.Api.Activity.Activity;
import org.springframework.data.repository.CrudRepository;

/** Container class (repository) for the SQL User Table itself */

public interface ActivityRepository extends CrudRepository<Activity,Integer>{

}