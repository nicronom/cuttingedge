package com.Restful.Api.ActivityAvailability;

import com.Restful.Api.Activity.Activity;
import com.Restful.Api.Booking.Booking;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/** The entity relationship of activity availability is declared below. */
@Entity
public class Availability {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer availabilityId;

    @Column(nullable = false)
    private String fromHour;

    @Column(nullable = false)
    private String untilHour;

    @Column(nullable = false)
    private String availableDay;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "activity_id")
    private Activity activity;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "availability")
    private List<Booking> bookings = new ArrayList<>();

    public Availability() {}

    public Availability(String from, String until, String day) {
        this.fromHour = from;
        this.untilHour = until;
        this.availableDay = day;
    }

    public Integer getActivityAvailabilityId() { return this.availabilityId; }

    public Activity getActivity() {
        return this.activity;
    }

    public String getFromHour() {
        return this.fromHour;
    }

    public String getUntilHour() {
        return this.untilHour;
    }

    public String getAvailableDay() {
        return this.availableDay;
    }

    public List<Booking> getBookings() { return this.bookings; }

    public void setActivityAvailabilityId(Integer id) {this.availabilityId = id;}

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setFromHour(String from) {
        this.fromHour = from;
    }

    public void setUntilHour(String until) {
        this.untilHour = until;
    }

    public void setAvailableDay(String day) {
        this.availableDay = day;
    }

    public void setBookings(List<Booking> bookings) { this.bookings = bookings; }

    public void addBooking(Booking booking) { this.bookings.add(booking); }

    public void removeBooking(Booking booking) { this.bookings.remove(booking); }

    // custom equals method for availability comparisons
    public boolean equals(Availability o) {
        if (o.getFromHour().equals(this.fromHour)
                && o.getUntilHour().equals(this.untilHour)
                && o.getAvailableDay().equals(this.availableDay)
            ) {
            return true;
        }
        return false;
    }

}
