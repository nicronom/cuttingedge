package com.Restful.Api.Booking;

import com.Restful.Api.ActivityAvailability.Availability;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

/**
 * Entity describing the use case scenario of booking a
 * particular activity class session, which is described
 * by many user emails taking many availaible class sessions. */
@Entity
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer bookingId;

    @Column(nullable = false)
    private String email;

    @JsonBackReference
    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "availability_id")
    private Availability availability;

    public Booking() {}

    public Booking(String email, Availability avail) {
        this.email = email;
        this.availability = avail;
    }

    public Integer getBookingId() { return this.bookingId; }

    public String getEmail() { return this.email; }

    public Availability getAvailability() { return this.availability; }

    public void setBookingId(Integer id) { this.bookingId = id; }

    public void setEmail(String email) { this.email = email;}

    public void setAvailability(Availability availability) { this.availability = availability; }
    
}
