INSERT INTO Users (email,password,first_name)
VALUES ('testUser1@abv.bg', 'myPassword1', 'User1');
INSERT INTO Users (email,password,first_name)
VALUES ('testUser2@abv.bg', 'myPassword2', 'User2');

INSERT INTO Activity (activity_name, img_url, activity_info)
VALUES ('Swimming', './images/swimming1.jpg', 'Our world-class facilities are ideal for you, whether you want to perfect your stroke with a personal coach, join classes or just unwind in the 8-lane, 25m pool. Our Gym offers an extensive swimming lesson programme.');
INSERT INTO Activity (activity_name, img_url, activity_info)
VALUES ('Boxing', './images/boxing.jpg', 'Get that core burning with our high-intensity boxing. Mixing traditional boxercise pad work with our own style of functional fitness, you will be testing your strength and endurance, as well as getting your heart pumping.');
INSERT INTO Activity (activity_name, img_url, activity_info)
VALUES ('Cardio', './images/cardio1.jpg', 'If you are looking to burn calories and test your levels of endurance, then our popular cardio class might just be for you.');
INSERT INTO Activity (activity_name, img_url, activity_info)
VALUES ('Pilates', './images/pilates1.jpg', 'Pilates is a form of low-impact exercise that aims to strengthen muscles while improving postural alignment and flexibility. Pilates moves tend to target the core, although the exercises work other areas of your body as well.');