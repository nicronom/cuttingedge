package com.Restful.Api;

import com.Restful.Api.users.users.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;

/** Test cases for the mocked version of the UsersController are defined
 *  below, as well as the sub-methods in UsersService that implement its
 *  logic. In other words the expected functionality of a
 *  simulated REST environment, prior to starting the actual
 *  HTTP server are examined within this class.
 *
 *  NOTE: some of the requests heavily rely simply on the implementations
 *  of the service methods. The test environment, however, only allows for
 *  one layer testing at a time which has both its advantages and disadvantages.
 *  Main disadvantage is that mock testing for post requests which inject the
 *  json body into a service method call are not possible without affecting
 *  separation of service and controller layers.*/
@WebMvcTest(controllers = UserController.class)
public class UsersServiceTests {

    // automatic initialisation of the test env mvc
    @Autowired
    MockMvc mockMvc;

    // mocked version of the user services
    @MockBean
    UsersService usersService;

    /*  Populating with a predefined user, should yield the
     *  appropriate response of the object from the service
     *  method, i.e. getUserByEmail(<email>) and the GET
     *  request /user/byEmail{<email>} should respond with the
     *  expected JSON object. */
    @Test
    public void testGetUser() throws Exception {
        // the instance of user we define
        Users testUser1 = new Users(
                "testGetUser@email.com",
                "testGetUserPassword",
                "TestGetUserFirstName",
                "TestGetUserLastName",
                "0123456789"
        );

        // tests the service method which should return the user instance
        given(usersService.getUserByEmail(testUser1.getEmail()))
                .willReturn(testUser1);

        // finally tests that the response of the request matches the user
        mockMvc.perform(get("/users/byEmail?email="
                + testUser1.getEmail())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("email",
                        is(testUser1.getEmail())))
                .andExpect(jsonPath("password",
                        is(testUser1.getPassword())))
                .andExpect(jsonPath("firstName",
                        is(testUser1.getFirstName())))
                .andExpect(jsonPath("lastName",
                        is(testUser1.getLastName())))
                .andExpect(jsonPath("isAdmin",
                        is(testUser1.getIsAdmin())))
                .andExpect(jsonPath("phone",
                        is(testUser1.getPhone())));
    }

    /*  The GET request for all users of the users table is tested
     *  below. Given defined set of users, tests whether the actual
     *  json object returned matches a list of these users objects. */
    @Test
    public void testGetAllUsers() throws Exception {
        // the user objects we are going to test with
        Users testUser1 = new Users(
                "testGetAllUser1@email.com",
                "testGetAllPassword1",
                "TestGetAllUser1",
                null,
                null
        );

        Users testUser2 = new Users(
                "testGetAllUser2@email.com",
                "testGetAllPassword2",
                "TestGetAllUser2",
                "TestGetAllUserLastName2",
                null
        );

        Users testUser3 = new Users(
                "testGetAllUser3@email.com",
                "testGetAllPassword3",
                "TestGetAllUser3",
                "TestGetAllUserLastName3",
                null
        );

        // the responses will be a list of objects, hence convertion is required
        List<Users> users = Arrays.asList(testUser1, testUser2, testUser3);

        // tests if the getAllUsers method in Service returns the correct object
        given(usersService.getAllUsers()).willReturn(users);

        // also tests if the according get request returns list of json objects
        mockMvc.perform(get("/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",
                        hasSize(3)))
                .andExpect(jsonPath("$[0].email",
                        is(testUser1.getEmail())))
                .andExpect(jsonPath("$[1].isAdmin",
                        is(testUser2.getIsAdmin())))
                .andExpect(jsonPath("$[2].phone",
                        is(testUser3.getPhone())));
    }


    /* Tests if an attempt to get a user which is not currently stored in
    *  the database yields a null object for the appropriate service method,
    *   and a NotFound status as the GET request response. */
    @Test
    public void testGetNonExistentUser() throws Exception{
        // no such user exists, the object returned should be a null reference
        given(usersService.getUserByEmail("thisEmailDoesNotExist@email.com")).
                willReturn(null);

        // no user with this email exists, the response should be 404 NotFound
        mockMvc.perform(get("/users/byEmail?email="
                + "thisEmailDoesNotExist@email.com")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testArchiveUser() {
        // adds a non-archived user record to the table
        Users testUser1 = new Users(
                "testUser1@email.com",
                "testPassword1",
                "TestUser1",
                null,
                null,
                false,
                false
        );

        // attempts to archive the user
        usersService.archiveUser(testUser1);
        // an archived user should have the according field value set to true
        given(usersService.getUserByEmail("testUser1@email.com"))
                .willReturn(new Users("testUser1@email.com",
                        "testPassword1",
                        "TestUser1",
                        null,
                        null,
                        true,
                        true));
    }

    /* The test below verifies that if a user record in the Users table exists,
    *  but is archived, then null object and 404 NotFound should be returned
    *  from the service method and GET request respectively. */
    @Test
    public void testGetArchivedUser() throws Exception{
        Users testUser1 = new Users(
                "thisEmailIsArchived@email.com",
                "thisPasswordIsArchived",
                "ThisUserIsArchived",
                null,
                null
        );

        // archive the user, such that the record is ignored by getters
        testUser1.setArchived(true);

        // the user exists but is archived, hence null should be returned
        given(usersService.getUserByEmail("thisEmailIsArchived@email.com")).
                willReturn(null);

        // the user exists but is archived, should respond with 404 NotFound
        mockMvc.perform(get("/users/byEmail?email="
                + "thisEmailIsArchived@email.com")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    /* Tests if the service method evaluates to false if an already
    *  existing user record is attempted to be added to the users table.*/
    @Test
    public void testAddingExistingUser() {
        // populate the users table with a record
        Users testUser1 = new Users(
                "thisUser1@email.com",
                "testPassword1",
                "TestUser1",
                null,
                null
        );

        // attempts to add the same record to the table
        given(usersService.addUser(testUser1)).willReturn(false);
    }

    /* Tests if a user can be successfully added to the database. */
    @Test
    public void testAddUser() {
        // null object should not be added
        given(usersService.addUser(null)).willReturn(false);

        // check that null parameter json object is not added
        given(usersService.
                addUser(new Users(
                        null,
                        null,
                        null,
                        null,
                        null))).willReturn(false);

        // perform the appropriate method for adding a user
        usersService.addUser(new Users(
                "addUser@email.com", "addUserPassword",
                "addUserFirstName", "addUserLastName",
                "123456789"));

        // check if the user added matches the expected object
        given(usersService.getUserByEmail("addUser@email.com")).willReturn(new Users(
                "addUser@email.com", "addUserPassword",
                "addUserFirstName", "addUserLastName",
                "123456789"));

        // add an archived user record
        usersService.addUser(new Users(
                "addUser2@email.com",
                "addUser2Password",
                "addUser2FirstName",
                "addUser2LastName",
                "223456789",
                false,
                true));

        // adding an email that already exists in the db records but is archived should not be possible
        given(usersService.addUser(new Users(
                "addUser2@email.com",
                "addUser3Password",
                "addUser3FirstName",
                "addUser3LastName",
                "323456789")))
                .willReturn(false);
    }

    // Tests if the admin field value of a user record is updated appropriately
    @Test
    public void testAdmin() {
        // adds user record to the table
        Users testUser1 = new Users(
                "testUser1@email.com",
                "testPassword1",
                "TestUser1",
                null,
                null
        );

        // tests if isAdmin is set to default value of false
        given(usersService.getUserByEmail("testUser1@email.com"))
                .willReturn(new Users("testUser1@email.com",
                        "testPassword1",
                        "TestUser1",
                        null,
                        null, false, false));

        // tests if isAdmin is set appropriately to the given record
        usersService.updateUserIsAdmin(testUser1,true);
        given(usersService.getUserByEmail("testUser1@email.com"))
                .willReturn(new Users("testUser1@email.com",
                        "testPassword1",
                        "TestUser1",
                        null,
                        null,
                        true,
                        false));
    }
}
