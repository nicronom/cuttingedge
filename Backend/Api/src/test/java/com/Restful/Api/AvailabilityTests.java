package com.Restful.Api;

import com.Restful.Api.Activity.Activity;
import com.Restful.Api.ActivityAvailability.Availability;
import com.Restful.Api.ActivityAvailability.AvailabilityController;
import com.Restful.Api.ActivityAvailability.AvailabilityService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.BDDMockito.given;

@WebMvcTest(controllers = AvailabilityController.class)
public class AvailabilityTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AvailabilityService availabilityService;

    @Test
    public void testGetAvailabilities() throws Exception {
        Activity testActivity = new Activity(
                "testName"
        );

        Availability avail = new Availability(
                "14:00",
                "18:00",
                "FRIDAY"
        );
        avail.setActivity(testActivity);

        Availability avail2 = new Availability(
                "13:00",
                "14:00",
                "TUESDAY"
        );
        avail2.setActivity(testActivity);

        testActivity.addAvailability(avail);
        testActivity.addAvailability(avail2);

        List<Availability> avails = Arrays.asList(avail, avail2);

        // expects to get the same predefined list
        given(availabilityService.getActivityAvailability(testActivity.getActivityName()))
                .willReturn(avails);

        // checks that invalid parameters return null as it should
        given(availabilityService.getActivityAvailability(null))
                .willReturn(null);
        given(availabilityService.getActivityAvailability("doesNotExist"))
                .willReturn(null);

        // check that the response body of the endpoint returns the expected values
        mockMvc.perform(get("/availability/byName?name="
                + testActivity.getActivityName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",
                        hasSize(2)))
                .andExpect(jsonPath("$[0].fromHour",
                        is(avail.getFromHour())))
                .andExpect(jsonPath("$[0].untilHour",
                        is(avail.getUntilHour())))
                .andExpect(jsonPath("$[0].availableDay",
                        is(avail.getAvailableDay().toString())))
                .andExpect(jsonPath("$[1].fromHour",
                        is(avail2.getFromHour())))
                .andExpect(jsonPath("$[1].untilHour",
                        is(avail2.getUntilHour())))
                .andExpect(jsonPath("$[1].availableDay",
                        is(avail2.getAvailableDay().toString())));

        // finally check that invalid activity name returns 404 NotFound
        mockMvc.perform(get("/availability/byName?name="
                + "doesNotExist")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    // Tests whether an invalid input cannot be issued to addAvailability
    @Test
    public void testAddAvail() {
        Activity testActivity = new Activity(
                "testName"
        );

        Availability avail = new Availability(
                "14:00",
                "16:00",
                "NotDayOfTheWeek");

        Availability avail2 = new Availability(
                "14:00",
                null,
                "NotDayOfTheWeek");

        given(availabilityService.addActivityAvailability(testActivity.getActivityName(),avail))
                .willReturn(false);

        given(availabilityService.addActivityAvailability(testActivity.getActivityName(),null))
                .willReturn(false);

        given(availabilityService.addActivityAvailability(testActivity.getActivityName(), avail2))
                .willReturn(false);
    }

    @Test
    public void testDeleteAvail() {
        Activity testActivity = new Activity(
                "testName"
        );

        Availability avail = new Availability(
                "14:00",
                "18:00",
                "FRIDAY"
        );
        avail.setActivity(testActivity);

        Availability avail2 = new Availability(
                "13:00",
                "14:00",
                "TUESDAY"
        );
        avail2.setActivity(testActivity);

        testActivity.addAvailability(avail);
        testActivity.addAvailability(avail2);

        // the expected return list, should be the list containing only avail2
        List<Availability> avails = Arrays.asList(avail2);
        // deletes one of the records
        availabilityService.deleteActivityAvailability(testActivity.getActivityName(), avail);
        // attempts to delete the availability of a non existent record
        availabilityService.deleteActivityAvailability("doesNotExist", avail2);
        availabilityService.deleteActivityAvailability(null, avail2);
        availabilityService.deleteActivityAvailability(testActivity.getActivityName(), null);

        // check that the availability was successfully removed from the db
        given(availabilityService.getActivityAvailability(testActivity.getActivityName()))
                .willReturn(avails);
    }
}
