package com.Restful.Api;

import com.Restful.Api.cardPayment.CardPayment;
import com.Restful.Api.cardPayment.CardPaymentController;
import com.Restful.Api.cardPayment.CardPaymentService;
import com.Restful.Api.paymentReceipt.PaymentReceipt;
import com.Restful.Api.users.users.Users;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.mockito.BDDMockito.given;

@WebMvcTest(controllers = CardPaymentController.class)
public class CardPaymentTests {
    // automatic initialisation of the test env mvc
    @Autowired
    MockMvc mockMvc;

    // mocked version of the cardPayment services
    @MockBean
    CardPaymentService cardPaymentService;

    @Test
    void testGetCardPayment() throws Exception {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Date  date1 = format.parse ( "2009-12-31" );
        Date  date2 = format.parse ( "2010-2-20" );

        // the instance of user we define
        Users testUser1 = new Users(
                "testGetUser@email.com",
                "testGetUserPassword",
                "TestGetUserFirstName",
                "TestGetUserLastName",
                "0123456789"
        );

        //Creating instances of payment receipts to test card payment returns
        PaymentReceipt testPaymentReceipt1 = new PaymentReceipt(
                "100.5",
                date1
        );
        testPaymentReceipt1.setUser(testUser1);

        PaymentReceipt testPaymentReceipt2 = new PaymentReceipt(
                "43.7",
                date2
        );
        testPaymentReceipt2.setUser(testUser1);
        testUser1.setPaymentReceipts(new ArrayList<>(Arrays.asList(testPaymentReceipt1,testPaymentReceipt2)));

        CardPayment cardPayment = new CardPayment("08786785645443");
        cardPayment.setPaymentReceipt(testPaymentReceipt1);
        testPaymentReceipt1.setCardPayment(cardPayment);

        String email = testUser1.getEmail();
        String str = testPaymentReceipt1.getAmount();
        Date date = testPaymentReceipt1.getDateIssued();
        cardPaymentService.getCardPayment(email ,str, date);
        // tests the service method which should return the cardPayment instance
        given(cardPaymentService.getCardPayment(testUser1.getEmail(),
                testPaymentReceipt1.getAmount(),
                testPaymentReceipt1.getDateIssued()))
                .willReturn(cardPayment);

        // tests the service method which should return the null as email does not exist
        given(cardPaymentService.getCardPayment(testUser1.getEmail(), null, date1))
                .willReturn(null);
    }
}

