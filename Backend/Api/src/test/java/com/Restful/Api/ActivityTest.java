package com.Restful.Api;

import com.Restful.Api.Activity.Activity;
import com.Restful.Api.Activity.ActivityController;
import com.Restful.Api.Activity.ActivityService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = ActivityController.class)
public class ActivityTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ActivityService activityService;

    @Test
    void testGetAllActivities() throws Exception {
        // creating activity objects that we are going to test
        Activity testActivity1 = new Activity(
                "testGetAllActivities1"
        );

        Activity testActivity2 = new Activity(
                "testGetAllActivities2",
                "/img/activityImage.png"
        );

        Activity testActivity3 = new Activity(
                "testGetAllActivities3"
        );

        // the responses will be a list of objects, hence convertion is required
        List<Activity> activities = Arrays.asList(testActivity1,testActivity2,testActivity3);

        //tests if getAllActivities method in ActivityService return the correct objects.
        given(activityService.getAllActivities()).willReturn(activities);

        //tests if the get request returns a list of json objects
        mockMvc.perform(get("/activities")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect((jsonPath("$",
                        hasSize(3))))
                .andExpect(jsonPath("$[0].activityName",
                        is(testActivity1.getActivityName())))
                .andExpect(jsonPath("$[1].activityName",
                        is(testActivity2.getActivityName())))
                .andExpect(jsonPath("$[1].imgUrl",
                        is(testActivity2.getImgUrl())))
                .andExpect(jsonPath("$[2].activityName",
                        is(testActivity3.getActivityName())));

    }

    @Test
    void testGetActivityByName() throws Exception {

        Activity testActivity1 = new Activity(
                "testActivityName",
                "/img/activityImage.png"
        );

        String name = testActivity1.getActivityName();

        // tests the service method which should return the user instance
        given(activityService.getActivityByName(testActivity1.getActivityName()))
                .willReturn(testActivity1);
            //tests if the get request returns a list of json objects
        mockMvc.perform(get("/activities/byName?name="
        + testActivity1.getActivityName())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("activityName",
                is((testActivity1.getActivityName()))))
        .andExpect(jsonPath("imgUrl",
                is(testActivity1.getImgUrl())));
    }


    @Test
    void testArchiveActivity() {
        //adds an unacrchived activity
        Activity testActivity1 = new Activity(
                "testActivity1",
                false
        );

        activityService.archiveActivity(testActivity1);
        given(activityService.getActivityByName("testActivity1"))
                .willReturn(new Activity("testActivity1",
                        true));

    }


    /* The test below verifies that if an activity record in the Activity table exists,
     *  but is archived, then null object and 404 NotFound should be returned
     *  from the service method and GET request respectively. */

    @Test
    void testGetArchivedActivity() throws Exception {
        Activity testActivity1 = new Activity(
                "testActivity1",
                false
        );
        // archive the activity, such that the record is ignored by getters
        testActivity1.setArchived(true);

        // the activity exists but is archived, hence null should be returned
        given(activityService.getActivityByName("testActivity1")).willReturn(null);

        // the activity exists but is archived, should respond with 404 NotFound
        mockMvc.perform(get("/activities/byName?name=" + "testActivity1")
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
    }


    @Test
    void testAddActivity() {

        given(activityService.addActivity(null)).willReturn(false);

        given(activityService.addActivity(new Activity(null,false))).willReturn(false);

        // performs the appropriate method for adding an activity
        activityService.addActivity(new Activity("testActivity1"));

        given(activityService.getActivityByName("testActivity1")).willReturn(new Activity("testActivity1"));

        // adds an archived activity
        activityService.addActivity(new Activity("testActivity2", true));

        // adding an name that already exists in the db records but is archived should not be possible
        given(activityService.addActivity(new Activity("testActivity2"))).willReturn(false);

    }
}