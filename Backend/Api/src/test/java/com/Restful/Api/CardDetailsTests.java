package com.Restful.Api;

import com.Restful.Api.cardDetails.CardDetails;
import com.Restful.Api.cardDetails.CardDetailsController;
import com.Restful.Api.cardDetails.CardDetailsService;
import com.Restful.Api.users.users.Users;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.mockito.BDDMockito.given;

/**
 * Test cases for card details mvc classes and their endpoints
 * are defined below. */
@WebMvcTest(controllers = CardDetailsController.class)
public class CardDetailsTests {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    CardDetailsService cardDetailsService;

    // Given a user email with a recorded card detail in the
    // database, when queried the appropriate service method and
    // endpoint should return the card details of that user.
    // Invalid or non existent details/user should be handled appropriately.
    @Test
    void testGetCardDetails() throws Exception {

        // simulates the jdbc handling
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse ( "2009-12-31" );

        Users testUser = new Users(
                "testUser1@email.com",
                "testUserPassword1",
                "Testname",
                "Testlastname",
                "0123456789"
        );

        CardDetails details = new CardDetails(
                "Testname",
                "0123012301230123",
                "123",
                date
        );

        // sets the appropriate references
        details.setUser(testUser);
        testUser.setCardDetails(details);

        // tests if the card details of that user are successfully returned
        given(cardDetailsService.getCardDetails(testUser.getEmail()))
                .willReturn(details);
        mockMvc.perform(get("/cardDetails/byEmail?email="
                + testUser.getEmail())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("cardHolderName",
                        is(details.getCardHolderName())))
                .andExpect(jsonPath("cardNumber",
                        is(details.getCardNumber())))
                .andExpect(jsonPath("cvv",
                        is(details.getCvv())));

        // Attempting to get the details of a non existent user
        // or a null user should not be possible.
        given(cardDetailsService.getCardDetails("nonExistentEmail@email.com"))
                .willReturn(null);
        given(cardDetailsService.getCardDetails(null))
                .willReturn(null);

        given(cardDetailsService.getCardDetails(testUser.getEmail()))
                .willReturn(details);
        mockMvc.perform(get("/cardDetails/byEmail?email="
                + "nonExistentEmail@email.com")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    // Tests if user's existing card details are appropriately
    // updated in the database by the given methods. The respective
    // service method and api endpoint is tested also.
    @Test
    void testChangeCardDetails() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse ( "2009-12-31" );

        Users testUser = new Users(
                "testUser1@email.com",
                "testUserPassword1",
                "Testname",
                "Testlastname",
                "0123456789"
        );

        CardDetails details = new CardDetails(
                "Testname",
                "0123012301230123",
                "123",
                date
        );

        details.setUser(testUser);
        testUser.setCardDetails(details);
        // changes the details using the service method,
        // valid parameters are set hence it should be possible
        given(cardDetailsService.updateCardDetails(testUser.getEmail(),
                new CardDetails("Testname",
                        "0123012301230123",
                        "456",
                        date)))
                .willReturn(true);
        // then checks if it is updated in the database
        given(cardDetailsService.getCardDetails(
                testUser.getEmail()))
                    .willReturn(new CardDetails("Testname",
                            "0123012301230123",
                            "456",
                            date));
    }

    // Tests if card details of a user can be successfully archived
    // and not displayed further for that user. In addition, since
    // details would exist that way, adding a new card detail should
    // overwrite the previous details.
    @Test
    void testDeleteCardDetails() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse ( "2009-12-31" );

        Users testUser = new Users(
                "testUser1@email.com",
                "testUserPassword1",
                "Testname",
                "Testlastname",
                "0123456789"
        );

        CardDetails details = new CardDetails(
                "Testname",
                "0123012301230123",
                "123",
                date
        );

        details.setUser(testUser);
        testUser.setCardDetails(details);
        // archiving the details should be possible
        given(cardDetailsService.archiveDetails(testUser.getEmail()))
                .willReturn(true);
        // check that the details are no longer returned
        given(cardDetailsService.getCardDetails(testUser.getEmail()))
                .willReturn(null);

        // finally check that the archived details are
        // overwritten, when trying to update the details
        // of that user
        cardDetailsService.updateCardDetails(testUser.getEmail(), new CardDetails(
                "Testname",
                "0123012301230123",
                "456",
                date
        ));
        given(cardDetailsService.getCardDetails(testUser.getEmail()))
                .willReturn(new CardDetails(
                        "Testname",
                        "0123012301230123",
                        "456",
                        date
                ));
    }
}
