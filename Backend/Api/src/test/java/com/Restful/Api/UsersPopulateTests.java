package com.Restful.Api;

import com.Restful.Api.users.users.Users;
import com.Restful.Api.users.users.UsersRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/** Test cases for all Users related MVC classes are defined below*/
@DataJpaTest
public class UsersPopulateTests {

    @Autowired
    private UsersRepository usersRepository;

    // Checks if the database is populated from a file on startup
    @Test
    void testLoadData() {

        List<Users> users = new ArrayList<>();
        Iterable<Users> usersIterable = usersRepository.findAll();
        for (Users user: usersIterable) {
            users.add(user);
        }

        // The table users should not be empty if data was populated
        assertTrue((users.size() > 0));
    }
}
