
package com.Restful.Api;

import com.Restful.Api.paymentReceipt.*;

import com.Restful.Api.users.users.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.BDDMockito.given;

@WebMvcTest(controllers = PaymentReceiptController.class)
public class PaymentReceiptTests {

    @Autowired
    MockMvc mockMvc;

    // Mocked version of the user services
    @MockBean
    PaymentReceiptService paymentReceiptService;

    @Test
    public void testGetPaymentReceipts() throws Exception {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");


        // the user objects we are going to test with
        Users testUser = new Users(
                "testGetAllUser1@email.com",
                "testGetAllPassword1",
                "TestGetAllUser1",
                null,
                null
        );

        Date  date1 = format.parse ( "2009-12-31" );
        Date  date2 = format.parse ( "2010-2-20" );

        PaymentReceipt testPaymentReceipt1 = new PaymentReceipt(
                "100.5",
                date1
        );
        testPaymentReceipt1.setUser(testUser);

        PaymentReceipt testPaymentReceipt2 = new PaymentReceipt(
                "43.7",
                date2
        );
        testPaymentReceipt2.setUser(testUser);

        List<PaymentReceipt> paymentReceipts = Arrays.asList(testPaymentReceipt1, testPaymentReceipt2);
        ArrayList<PaymentReceipt> transformed = new ArrayList<>(paymentReceipts);
        testUser.setPaymentReceipts(transformed);

        //test if the getAllPaymentReceipts return all the added receipts for this user.
        given(paymentReceiptService.getAllPaymentReceipt("testGetAllUser1@email.com")).willReturn(paymentReceipts);

        //Given a user email it returns the latest receipts for that user
        given(paymentReceiptService.getLatestPaymentReceipt("testGetAllUser1@email.com"))
                .willReturn(paymentReceipts.get(1));

        mockMvc.perform(get(
                "/latestPaymentReceipt/byEmail?email="
                        +testPaymentReceipt2.getUser().getEmail())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("amount",
                        is(testPaymentReceipt2.getAmount())));
    }

    /* Tests if a paymentReceipt can be successfully added to the database. */
    @Test
    public void testAddPaymentReceipt() throws Exception {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        // the user objects we are going to test with
        Users testUser = new Users(
                "testGetAllUser1@email.com",
                "testGetAllPassword1",
                "TestGetAllUser1",
                null,
                null
        );

        Date  date1 = format.parse ( "2009-12-31" );
        Date  date2 = format.parse ( "2010-2-20" );

        // null object should not be added
        given(paymentReceiptService.addPaymentReceipt(testUser.getEmail(),null)).willReturn(false);

        PaymentReceipt testPaymentReceipt1 = new PaymentReceipt("8543.5345", date1);

        // perform the appropriate method for adding a paymentReceipts
        given(paymentReceiptService.addPaymentReceipt("testGetAllUser1@email.com", testPaymentReceipt1)).willReturn(true);

        given(paymentReceiptService.addPaymentReceipt("testGetAllUser1@email.com",new PaymentReceipt("8768.97", date2))).willReturn(true);
    }
}



